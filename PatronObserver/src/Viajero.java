import Interfases.Observador;

public class Viajero implements Observador{
    private String nombre;
    private Vuelo vuelo;
    public Viajero(String nombre, Vuelo vuelo) {
        super();
        this.nombre = nombre;
        this.vuelo = vuelo;
    }
    public void notificar(String mensaje) {
        System.out.println(nombre + "<-- Notificar: " + mensaje);
       
       
    }
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Vuelo getVuelo() {
		return vuelo;
	}
	public void setVuelo(Vuelo vuelo) {
		this.vuelo = vuelo;
	}
}