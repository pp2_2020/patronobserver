
public class PruebaVuelo {

	public static void main(String[] args) {
		
		Vuelo vuelo = new Vuelo("IB123 destino Par�s");
		Equipaje equipaje=new Equipaje();
		Viajero oscar = new Viajero("Oscar", vuelo);
		
		Viajero pepe = new Viajero("Pepe", vuelo);
		
		
		equipaje.suscribirObservador(oscar);	
		
		vuelo.suscribirObservador(oscar);		
		vuelo.suscribirObservador(pepe);
		
		vuelo.setUltimoSuceso("Llegada del vuelo.");
		equipaje.setUltimoSuceso("Equipaje en bandeja.");
		//vuelo.eliminarObservador(pepe);
		vuelo.setUltimoSuceso("Salida de viajeros");

	}

}
