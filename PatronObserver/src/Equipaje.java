import Interfases.Observador;

public class Equipaje extends Observado{
	Viajero viajero;
	private String ultimoSuceso;
	private String codigoDestino;
	
	public Equipaje() {
		
		
	}
	
	public void suscribirObservador(Observador observador) {
        observadores.add(observador);
        etiquetarEquipaje((Viajero)observador); 
    }
	
	public void etiquetarEquipaje(Viajero viajero) {
	this.viajero=viajero;
	this.codigoDestino=viajero.getVuelo().getCodigoDestino();
	}

	 public String getUltimoSuceso() {
	        
			return codigoDestino + ":" + ultimoSuceso;
	    }
	    public void setUltimoSuceso(String ultimoSuceso) {
	        this.ultimoSuceso = ultimoSuceso;
	        notificarObservadores("Equipaje:"+viajero.getNombre()+":"+ultimoSuceso);
	    }
	
	

}
