import java.util.ArrayList;
import java.util.List;

import Interfases.Observador;

public class Observado {
    protected List<Observador> observadores;
    public Observado() {
        super();
        observadores = new ArrayList<Observador>();
    }
    public void suscribirObservador(Observador observador) {
        observadores.add(observador);
    }
    public void eliminarObservador(Observador observador) {
        observadores.remove(observador);
    }
    public void notificarObservadores(String mensaje) {
        for(Observador observador: observadores) observador.notificar(mensaje);
    }
}