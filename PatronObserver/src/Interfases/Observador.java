package Interfases;

public interface Observador {
    void notificar(String mensaje);
}